import tw from "twin.macro";
import styled from 'styled-components';
import {makeStyles} from "@material-ui/core";

const useAuthStyles = () => {
    const StyledSectionBody = styled.div.attrs({
        className: 'section-body'
    })`
      ${tw` `}
    `;

    const StyledNavSection = styled.div.attrs({
        className: 'section-nav'
    })`
      ${tw` hidden`}
      &.active {
        ${tw` block`}
      }
    `;

    const StyledAuth = styled.div.attrs({
        className: 'auth'
    })`
      ${tw` lg:py-12 py-4 relative`}
    `;

    const StyledContent = styled.div.attrs({
        className: 'content-auth'
    })`
      ${tw` max-w-screen-lg mx-auto flex flex-col items-center justify-center`}
    `;

    const StyledFormLogin = styled.form.attrs({
        className: "StyledFormLogin space-y-6 lg:space-y-4 py-8",
    })`
      ${tw` lg:px-40 px-12`}
      .StyledWarpperInput {
        input {
          ${tw` w-full`}
        }
      }
    `

    const classesLogin = makeStyles({
        input: {
            width: '100%'
        },
        button: {
            width: '50%',
            backgroundColor: '#182551',
            color: "#fff",
            fontSize: "1vw",
            borderRadius: 10,
            '&:hover': {
                backgroundColor: '#182551'
            }
        }
    });

    const classesRegister = makeStyles({
       'input > input': {
           height: 30
       }
    });

    return {
        StyledSectionBody,
        StyledNavSection,
        StyledAuth,
        StyledContent,
        StyledFormLogin,
        classesLogin,
        classesRegister,
    }
}

export default useAuthStyles;