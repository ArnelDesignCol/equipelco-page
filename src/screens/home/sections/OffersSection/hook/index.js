import React, {useEffect} from "react";
import useActions from "../../../../../store/actions";
import useSelectors from "../../../../../store/selectors";
import {useSelector} from "react-redux";

const useOffersSection = () => {
    // Actions
    const {dispatch, useHomeActions} = useActions();
    const {actGetOffers} = useHomeActions();

    // Selectors
    const {useHomeSelectors} = useSelectors();
    const {offersSelector} = useHomeSelectors();
    const offers = useSelector(offersSelector);

    // Effects
    useEffect(() => {
        dispatch(actGetOffers);
    }, [dispatch]);

    return {
        offers
    }
}

export default useOffersSection;