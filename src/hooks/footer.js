import {useEffect, useState} from "react";
import useActions from "../store/actions";
import useSelectors from "../store/selectors";
import {useSelector} from "react-redux";

const useFooter = () => {
    // States
    const [hover, setHover] = useState({id: 0, hover: false});

    // Actions
    const { dispatch, useFooterActions } = useActions();
    const { actGetContactDetail, actGetPaymentMethods, actGetServicesDetail } = useFooterActions();

    // Selectors
    const { useFooterSelectors } = useSelectors();
    const { paymentMethodsSelector, contactDetailsSelector, servicesDetailsSelector } = useFooterSelectors();
    const paymentMethods = useSelector(paymentMethodsSelector);
    const contactDetail = useSelector(contactDetailsSelector);
    const serviceDetail = useSelector(servicesDetailsSelector);

    // Effects
    useEffect(() => {
        dispatch(actGetPaymentMethods);
        dispatch(actGetContactDetail);
        dispatch(actGetServicesDetail);
    }, [dispatch]);

    // Handlers
    const handlerHover = (type, id) => {
      if(type === 'over'){
          setHover({id: id, hover: true});
      }else{
          setHover({id: 0, hover: false});
      }
    }

  return {
      paymentMethods,
      contactDetail,
      serviceDetail,
      hover,
      handlerHover
  }
}

export default useFooter;