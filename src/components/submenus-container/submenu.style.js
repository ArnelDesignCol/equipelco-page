import {makeStyles} from "@material-ui/core";

const useStyleSubmenu = makeStyles({
    container: {
        backgroundColor: "#182551",
        position: 'fixed',
        bottom: '11vw',
        padding: '3% 4%',
        fontSize: '2vw',
        width: '100%',
        borderRadius: 10,
        border: '1px solid #fff',
        '@media (max-width: 1239px)': {
            bottom: '8vw',
            padding: '2% 4%',
        },
        '@media (max-width: 1024px)': {
            bottom: '9vw',
            padding: '2% 4%',
        },
        '@media (max-width: 768px)': {
            bottom: '10vw',
            padding: '2% 4%',
        },
        '@media (max-width: 540px)': {
            bottom: '18vw'
        },

    }
})
export default useStyleSubmenu;

