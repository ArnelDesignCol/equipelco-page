const defaultTheme = require('tailwindcss/defaultTheme');
const plugin = require('tailwindcss/plugin');

module.exports = {
  purge: {
    content: ["./{layouts,pages,components}/**/*.{js,jsx,ts,tsx}"],
  },
  darkMode: false,
  theme: {
    extend: {
      colors: {
        primary: "#182551"
      }
    },
  },
  variants: {
    translate: ["responsive", "hover", "focus", "group-hover"],
  },
  plugins: [
      require('@tailwindcss/custom-forms'),
      plugin(function ({addComponents, addBase, theme}){
        addBase({
          div: {
            borderStyle: 'solid',
          }
        });
        const components = {};
        addComponents(components)
      }),
  ],
}
