import { combineReducers } from "@reduxjs/toolkit";
import useGeneralReducer from "./general";
import useAuthReducers from "./auth";
import useFooterReducers from "./footer";
import useHomeReducers from "./home";

const useCombineReducers = () => {
    const {socialLinks} = useGeneralReducer();
    const {authCredencials, departaments, cities} = useAuthReducers();
    const { paymentMethods, contactDetails, servicesDetails } = useFooterReducers();
    const {carousel, brands, servicesSection, offers, productsHome} = useHomeReducers();

    return combineReducers({
        socialLinks,
        authCredencials,
        departaments,
        cities,
        paymentMethods,
        contactDetails,
        servicesDetails,
        brands,
        carousel,
        servicesSection,
        offers,
        productsHome
    });
}

export default useCombineReducers;