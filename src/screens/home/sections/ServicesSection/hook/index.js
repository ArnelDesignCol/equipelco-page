import useActions from "../../../../../store/actions";
import useSelectors from "../../../../../store/selectors";
import {useSelector} from "react-redux";
import {useEffect} from "react";

const useServicesSection = () => {
    // Actions
    const {dispatch, useHomeActions} = useActions();
    const {actGetServicesSection} = useHomeActions();

    // Selectors
    const {useHomeSelectors} = useSelectors();
    const {servicesSelector} = useHomeSelectors();
    const services = useSelector(servicesSelector);

    useEffect(() => {
        dispatch(actGetServicesSection);
    }, [dispatch]);

    return {
        services
    }
}

export default useServicesSection;