import useCreateSelector from "../createSelector";
import _ from "lodash";

const useFooterSelectors = () => {
    const { createSelector } = useCreateSelector();

    const paymentMethodsSelector = createSelector(
        state => state.paymentMethods,
        paymentMethods => {
            try {
                const { entities, results } = paymentMethods;
                return _.map(results.paymentMethods, (m) => entities.paymentMethod[m]);
            } catch (e) {
                return [];
            }
        }
    );

    const contactDetailsSelector = createSelector(
        state => state.contactDetails,
        contactDetails => {
            try {
                const { entities, results } = contactDetails;
                return _.map(results.contactDetails, (m) => entities.contactDetail[m]);
            } catch (e) {
                return [];
            }
        }
    );

    const servicesDetailsSelector = createSelector(
        state => state.servicesDetails,
        servicesDetails => {
            try {
                const { entities, results } = servicesDetails;
                return _.map(results.servicesDetails, (m) => entities.servicesDetail[m]);
            } catch (e) {
                return [];
            }
        }
    );

    return {
        paymentMethodsSelector,
        contactDetailsSelector,
        servicesDetailsSelector,
    }
}

export default useFooterSelectors;