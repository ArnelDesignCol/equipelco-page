import tw from "twin.macro";
import styled from "styled-components";

const useTabStyle = () => {
    const StyledTab = styled.div.attrs({
        className: 'Tab transition duration-300 ease-in-out'
    })`
      &.MuiTab-root {
        min-height: 64px;
        font-family: "Raleway", sans-serif;
        ${tw` text-xs font-bold lg:text-lg pb-2 lg:pb-4 opacity-100 lg:mr-4 mr-2 max-w-none`}
        
        &.active {
          &:before {
            ${tw `bg-primary`}
          }
          
          .MuiTab-wrapper {
            ${tw` bg-primary`}
          }
        }
        
        &:last-child {
          ${tw`lg:mr-0 mr-0`}
        }
        
        &:hover {
          ${tw`text-black`}
        }
        
        &:focus {
          ${tw` outline-none`}
        }
        
        &.Mui-selected {
          ${tw`text-primary`}
          
          .MuiTab-wrapper {
            font-weight: 800;

          }
          
          &:before {
            ${tw` opacity-0`}
          }
        }
        
        &:before {
          content: "";
          width: 100%;
          height: 12px;
          border-radius: 27px;
          position: absolute;
          background: none;
          bottom: 0;
          
          @media screen and (max-width: 1239px){
            height: 6px;
          }
        }
        
        .MuiTab-wrapper {
          ${tw` flex-row text-sm lg:text-lg mb-2 lg:mb-4`}
        }
      }
    `;

    return {
        StyledTab
    };
}

export default useTabStyle;