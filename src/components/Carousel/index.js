import React from "react";
import Slider from "react-slick";

// Styles
import 'slick-carousel/slick/slick.css';
import "slick-carousel/slick/slick-theme.css";

const Carousel = props => {
    const {
        children,
        dots,
        arrows,
        responsive,
        slidesToScroll,
        slidesToShow
    } = props;

    return (
        <Slider
            dots={dots}
            arrows={arrows}
            responsive={responsive}
            slidesToShow={slidesToShow}
            slidesToScroll={slidesToScroll}
        >
            {children}
        </Slider>
    );
}

export default Carousel;