import {makeStyles} from "@material-ui/core";

export const useStylesFormRegister = makeStyles({
    button: {
        fontFamily: "Raleway, sans-serif",
        fontSize: '1rem',
        padding: '10px 40px',
        backgroundColor: '#182551',
        color: "#fff",
        fontWeight: "bolder",
        textTransform: "Capitalize",
        "&:hover": {
            backgroundColor: "#293a73"
        }
    }
})