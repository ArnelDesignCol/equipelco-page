import React from 'react';
import useComponents from "../../../../components";
import useCarouselInitial from "./hook";
import _ from "lodash";
import useCarouselInitialStyles from "./carousel-initial.style";

const CarouselInitial = () => {
    const {Carousel} = useComponents();

    const {carousel} = useCarouselInitial();

    const {
        StyledImageCarousel
    } = useCarouselInitialStyles();
    return (
        <Carousel
            dots={false}
            arrows={false}
            slidesToShow={1}
            slidesToScroll={1}
        >
            {
                _.map(carousel, (item, index) => (
                    <StyledImageCarousel key={index} src={item.image}/>
                ))
            }
        </Carousel>
    );
}

export default CarouselInitial;