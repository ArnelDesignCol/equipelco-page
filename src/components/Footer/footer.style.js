import styled from "styled-components";
import {makeStyles} from "@material-ui/core";

const useFooterStyles = () => {
    const StyledFooter = styled.footer.attrs({
        className: 'footer',
        id: 'footer'
    })`
      background-image: url("/images/footer/Mundo.png");
      background-size: cover;
      background-repeat: no-repeat;
      padding: 60px 0px;

      @media (max-width: 1023px) {
        background-color: #182551;
        background-image: none;
      }
    `;

    const StyledTitle = styled.h2.attrs({
        className: 'title__section__footer'
    })`
      color: #fff;
      text-transform: uppercase;
      font-family: Raleway, sans-serif;
      font-size: 1.5em;
      font-weight: bolder;
    `;

    const StyledPaymentMethod = styled.img.attrs({
        className: 'payment__methods__images'
    })`

    `;

    const StyledIcon = styled.i`
        font-size: 30px;
        color: #fff;
    `;

    const StyledDetail = styled.h3`
        color: #fff;
        font-family: Raleway, sans-serif;
        font-size: 1.17em;
        font-weight: 600;
      
        @media (max-width: 414px){
          font-size: 4vw;
        }
    `;

    const useStyles = makeStyles({
        rowPaymentMethods: {
            borderBottom: '2px solid #fff'
        },
        textList: {
            color: '#fff',
            fontFamily: 'Raleway, sans-serif',
            fontWeight: 600,
            fontSize: '1.17em'
        },
        contactClass: {
            borderRight: '2px solid #fff',
            '@media (max-width: 1279px)': {
                borderBottom: '2px solid #fff',
                borderRight: 'none',
                paddingBottom: '5%'
            },
            '@media (max-width: 959px)': {
                borderBottom: '2px solid #fff !important',
                borderRight: 'none !important',
                paddingBottom: '5%'
            }
        },
        servicesClass: {
            paddingLeft: '5%',
            borderRight: '2px solid #fff',
            '@media (max-width: 959px)': {
                borderRight: 'none !important',
                borderBottom: '2px solid #fff !important',
                paddingBottom: '5%',
                paddingTop: '5%',
            },
            '@media (max-width: 1279px)': {
                borderRight: '2px solid #fff',
                borderBottom: 'none',
                paddingBottom: '5%',
                paddingTop: '5%',
            }
        },
        usersClass: {
            paddingLeft: '5%',
            '@media (max-width: 959px)': {
                borderRight: 'none',
                borderBottom: '2px solid #fff',
                paddingBottom: '5%'
            },
            '@media (max-width: 1279px)': {
                borderBottom: 'none',
                borderRight: 'none',
                paddingBottom: '5%',
                paddingTop: '5%',
            }
        }
    });

    return {
        StyledFooter,
        StyledTitle,
        StyledPaymentMethod,
        StyledIcon,
        StyledDetail,
        useStyles,
    }
}

export default useFooterStyles;