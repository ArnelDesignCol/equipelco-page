import axios from "axios";
import PropTypes from 'prop-types';

const useAuthProviders = () => {
    const proLogin = ({email, password}) => {
        let consulta = ""
        consulta = consulta + "<PagXml>                                ";
        consulta = consulta + "   <CorXml>                             ";
        consulta = consulta + "      <TIPO>PARAMETRO</TIPO>            ";
        consulta = consulta + "      <PROCESO>0142</PROCESO>             ";
        consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";
        consulta = consulta + "      <CORREO>" + email + "</CORREO>               ";
        consulta = consulta + "      <CLAVE>" + password + "</CLAVE>                                         ";
        consulta = consulta + "   </CorXml>                             ";
        consulta = consulta + "</PagXml>                               ";

        return axios.post('https://syswork.azurewebsites.net/Servicios/sweb_carrito.asmx/SetData', "Parametros=" + consulta + "");
    }

    proLogin.propTypes = {
        email: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired
    }

    const proRegisterUser = (
        names,
        last_names,
        document,
        email,
        phone,
        address,
        department,
        city,
        company,
        postal_code,
        password
    ) => {
        let consulta = ""
        consulta = consulta + "<PagXml>                                                          ";
        consulta = consulta + "   <CorXml>                                                       ";
        consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                      ";
        consulta = consulta + "      <PROCESO>0141</PROCESO>                                     ";
        consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";
        consulta = consulta + "      <NOMBRES>" + names + "</NOMBRES>                           ";
        consulta = consulta + "      <APELLIDOS>" + last_names + "</APELLIDOS>                  ";
        consulta = consulta + "      <IDENTIFICACION>" + document + "</IDENTIFICACION>                 ";
        consulta = consulta + "      <CORREO>" + email + "</CORREO>               ";
        consulta = consulta + "      <TELEFONO>" + phone + "</TELEFONO>                            ";
        consulta = consulta + "      <DIRECCION>" + address + "</DIRECCION>         ";
        consulta = consulta + "      <DEPARTAMENTO>" + department.value + "</DEPARTAMENTO>                             ";
        consulta = consulta + "      <CIUDAD>" + city.value + "</CIUDAD>                                       ";
        consulta = consulta + "      <EMPRESA>" + company + "</EMPRESA>             ";
        consulta = consulta + "      <CODIGO_POSTAL>" + postal_code + "</CODIGO_POSTAL>                        ";
        consulta = consulta + "      <FLAG_FACTURA>1</FLAG_FACTURA>                              ";
        consulta = consulta + "      <CLAVE>" + password + "</CLAVE>                                        ";
        consulta = consulta + "      <REG>0</REG>                                                ";
        consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>                ";
        consulta = consulta + "   </CorXml>                                                      ";
        consulta = consulta + "</PagXml>                                                         ";

        return axios.post("https://syswork.azurewebsites.net/Servicios/sweb_carrito.asmx/SetData", "Parametros=" + consulta + "");
    }

    proRegisterUser.propTypes = {
        names: PropTypes.string.isRequired,
        last_names: PropTypes.string.isRequired,
        document: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        department: PropTypes.object.isRequired,
        city: PropTypes.object.isRequired,
        company: PropTypes.string.isRequired,
        postal_code: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired
    }

    const proGetCities = () => {
        const request = `
            <PagXml>
                <CorXml>
                    <TIPO>PARAMETRO</TIPO>
                    <CODIGO>0139</CODIGO>
                    <PAGINA>0</PAGINA>
                    <PAGINAS>0</PAGINAS>
                    <NOMBRE>PROCESO</NOMBRE>
                    <ORDEN>1</ORDEN>
                    <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>
                    <FILTRO><![CDATA[]]></FILTRO>
                </CorXml>
            </PagXml>
        `;

        const data = `Parametros=${request}`;
        return axios.post('https://syswork.azurewebsites.net/Servicios/sweb_carrito.asmx/FiltroBusquedaJson', data);
    }

    const proGetDepartaments = () => {
        const request = `
            <PagXml>
                <CorXml>
                    <TIPO>PARAMETRO</TIPO>
                    <CODIGO>0138</CODIGO>
                    <PAGINA>0</PAGINA>
                    <PAGINAS>0</PAGINAS>
                    <NOMBRE>PROCESO</NOMBRE>
                    <ORDEN>1</ORDEN>
                    <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>
                    <FILTRO><![CDATA[]]></FILTRO>
                </CorXml>
            </PagXml>
        `;

        const data = `Parametros=${request}`;
        return axios.post('https://syswork.azurewebsites.net/Servicios/sweb_carrito.asmx/FiltroBusquedaJson', data);
    }

    return {
        proLogin,
        proRegisterUser,
        proGetCities,
        proGetDepartaments,
    }
}

export default useAuthProviders;