import useProviders from "../../providers";
import {trackPromise} from "react-promise-tracker";

const useHomeServices = () => {
    const {useHomeProviders} = useProviders();
    const {proGetCarousel, proGetBrands, proGetServicesSection, proGetOffers, proGetProducts} = useHomeProviders();

    const serGetCarousel = () => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proGetCarousel()));
            } catch (e) {
                reject(e);
            }
        })
    }

    const serGetBrands = () => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proGetBrands()));
            } catch (e) {
                reject(e);
            }
        });
    }

    const serGetServicesSection = () => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proGetServicesSection()));
            } catch (e) {
                reject(e);
            }
        });
    }

    const serGetOffers= () => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proGetOffers()));
            } catch (e) {
                reject(e);
            }
        });
    }

    const serGetProducts= () => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proGetProducts()));
            } catch (e) {
                reject(e);
            }
        });
    }

    return {
        serGetCarousel,
        serGetBrands,
        serGetServicesSection,
        serGetOffers,
        serGetProducts,
    }
}

export default useHomeServices;