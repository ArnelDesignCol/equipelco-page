import React from 'react';
import useTabStyle from "./Tab.style";
import {Tab} from "@material-ui/core";
import PropTypes from 'prop-types';

const NavigationTab = props => {
    const {className, label, ...others} = props;
    const {StyledTab} = useTabStyle();
    return (
        <StyledTab
            as={Tab}
            disableRipple
            label={label}
            className={className}
            {...others}
        />
    );
}

NavigationTab.propTypes = {
    className: PropTypes.string,
    label: PropTypes.node.isRequired
}

NavigationTab.defaultProps = {
    className: 'Tab'
}

export default NavigationTab;