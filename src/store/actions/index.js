import {useDispatch} from "react-redux";
import useGeneralActions from "./general";
import useAuthActions from "./auth";
import useFooterActions from "./footer";
import useHomeActions from "./home";

const useActions = () => {
    const dispatch = useDispatch();

    return {
        dispatch,
        useGeneralActions,
        useAuthActions,
        useFooterActions,
        useHomeActions,
    }
}

export default useActions;