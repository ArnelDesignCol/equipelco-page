import createReducer from "../createReducer";
import {GET_CONTACT_DETAIL, GET_PAYMENT_METHODS, GET_SERVICES_DETAIL} from "../../types";

const useFooterReducers = () => {
    const paymentMethods = createReducer({
        entities: {},
        results: []
    }, {
        [GET_PAYMENT_METHODS](state, action){
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    const contactDetails = createReducer({
        entities: {},
        results: []
    }, {
        [GET_CONTACT_DETAIL](state, action){
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    const servicesDetails = createReducer({
        entities: {},
        results: []
    }, {
        [GET_SERVICES_DETAIL](state, action){
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    return {
        paymentMethods,
        contactDetails,
        servicesDetails,
    }
}

export default useFooterReducers;