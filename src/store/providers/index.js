import useGeneralProvider from "./general";
import useAuthProviders from "./auth";
import useFooterProviders from "./footer";
import useHomeProviders from "./home";

const useProviders = () => {
    return {
        useGeneralProvider,
        useAuthProviders,
        useFooterProviders,
        useHomeProviders,
    };
}

export default useProviders;