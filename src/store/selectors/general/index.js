import useCreateSelector from "../createSelector";
import _ from "lodash";

const useGeneralSelectors = () => {
    const {createSelector} = useCreateSelector();

    const socialLinksSelector = createSelector(
        state => state.socialLinks,
        socialLinks => {
            try {
                const {entities,results} = socialLinks;
                return _.map(results.socialLinks, (m) => entities.socialLink[m]);
            }catch (e) {
                return [];
            }
        }
    );

    return {
        socialLinksSelector,
    }
}

export default useGeneralSelectors;