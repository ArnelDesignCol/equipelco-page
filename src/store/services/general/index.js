import useProviders from "../../providers";

const useGeneralServices = () => {
    const {useGeneralProvider} = useProviders();
    const {proGetSocialLinks} = useGeneralProvider();

    const serGetSocialLinks = () => {
        return new Promise(async(resolve, reject) => {
            try {
                resolve(await proGetSocialLinks());
            }catch (error){
                reject(error);
            }
        })
    }

    return {
        serGetSocialLinks,
    }
}

export default useGeneralServices;