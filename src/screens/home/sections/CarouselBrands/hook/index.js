import useActions from "../../../../../store/actions";
import useSelectors from "../../../../../store/selectors";
import {useSelector} from "react-redux";
import {useEffect} from "react";

const useCarouselBrands = () => {
    // Actions
    const {dispatch, useHomeActions} = useActions();
    const {actGetBrands} = useHomeActions();

    // Selectors
    const {useHomeSelectors} = useSelectors();
    const {brandsSelector} = useHomeSelectors();
    const brands = useSelector(brandsSelector);

    // Effects
    useEffect(() => {
        dispatch(actGetBrands);
    }, [dispatch]);


    return {
        brands
    }
}

export default useCarouselBrands;