import React from "react";
import useCarouselBrandsStyles from "./carousel-brands.style";
import useComponents from "../../../../components";
import useCarouselBrands from "./hook";
import _ from "lodash";
import {Container} from "@material-ui/core";

const CarouselBrands = () => {
    const {
        StyledContainerCarouselBrands,
        StyledImageCarouselBrands
    } = useCarouselBrandsStyles();

    const {
        Carousel
    } = useComponents();

    const {
        brands
    } = useCarouselBrands();

    return (
        <StyledContainerCarouselBrands className="py-16">
            <Container>
                <Carousel
                    dots={false}
                    arrows={false}
                    slidesToShow={4}
                    slidesToScroll={4}
                    responsive={
                        [
                            {
                                breakpoint: 500,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    dots: false,
                                    arrows: false
                                }
                            },
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    infinite: true,
                                    dots: false,
                                    arrows: false
                                }
                            }
                        ]
                    }
                >
                    {
                        _.map(brands, (item, index) => (
                            <StyledImageCarouselBrands key={index} src={item.image}/>
                        ))
                    }
                </Carousel>
            </Container>
        </StyledContainerCarouselBrands>
    );
}

export default CarouselBrands;