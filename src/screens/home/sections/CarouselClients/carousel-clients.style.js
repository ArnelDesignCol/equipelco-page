import styled from "styled-components";

const useCarouselClientsStyles = () => {
    const StyledTitleSections = styled.h2.attrs({
        className: 'title-section'
    })`
      color: #000;
      font-size: 1.5em;
      text-align: center;
      text-transform: uppercase;
      font-weight: 600;
      padding-bottom: 60px;
      font-family: Raleway, sans-serif;
    `;

    const StyledContainerCarouselClients = styled.section.attrs({
        className: 'carousel__clients'
    })`
      background-color: #e6e6e6;
    `;

    const StyledImageCarouselSections = styled.img.attrs({
        className: 'image__carousel__clients'
    })`
        width: 90%;
      padding: 0px 10px;
    `;
  return {
      StyledTitleSections,
      StyledContainerCarouselClients,
      StyledImageCarouselSections
  }
}

export default useCarouselClientsStyles;