import useProviders from "../../providers";
import PropTypes from "prop-types";
import {trackPromise} from "react-promise-tracker";

const useAuthServices = () => {
    const {useAuthProviders} = useProviders();
    const {proLogin, proRegisterUser, proGetDepartaments, proGetCities} = useAuthProviders();

    const serLogin = ({email, password}) => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proLogin({email, password})));
            } catch (error) {
                reject(error);
            }
        });
    }

    serLogin.propTypes = {
        email: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired
    }

    const serRegisterUser = (
        names,
        last_names,
        document,
        email,
        phone,
        address,
        department,
        city,
        company,
        postal_code,
        password
    ) => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proRegisterUser(names, last_names, document, email, phone, address, department, city, company, postal_code, password)));
            } catch (error) {
                reject(error);
            }
        });
    }

    serRegisterUser.propTypes = {
        names: PropTypes.string.isRequired,
        last_names: PropTypes.string.isRequired,
        document: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        department: PropTypes.object.isRequired,
        city: PropTypes.object.isRequired,
        company: PropTypes.string.isRequired,
        postal_code: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired
    }

    const serGetDepartaments = () => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proGetDepartaments()));
            } catch (error) {
                reject(error);
            }
        });
    }

    const serGetCities = () => {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await trackPromise(proGetCities()));
            } catch (error) {
                reject(error);
            }
        });
    }

    return {
        serLogin,
        serRegisterUser,
        serGetDepartaments,
        serGetCities
    }
}

export default useAuthServices;