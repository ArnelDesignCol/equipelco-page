import createReducer from "../createReducer";
import {GET_SOCIAL_LINKS} from "../../types";

const useGeneralReducer = () => {
    const socialLinks = createReducer({
        entities: {},
        results: []
    },{
        [GET_SOCIAL_LINKS](state, action){
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    })
    return {
        socialLinks,
    }
}

export default useGeneralReducer;