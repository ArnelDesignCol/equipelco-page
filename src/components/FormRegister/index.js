import React from "react";
import {Button, Container, Grid, TextField} from "@material-ui/core";
import useAuth from "../../hooks/auth";
import Select from "react-select";
import '../../styles/registerForm.css';
import {Controller} from 'react-hook-form'
import {useStylesFormRegister} from "./formRegister.style";

const FormRegister = () => {
    const {
        register,
        handleSubmit,
        registerUser,
        departaments,
        cities,
        control,
        filterCities,
        selectedCities
    } = useAuth();

    const classes = useStylesFormRegister();

    return (
        <Container>
            <Grid container>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('names')} label="Nombres" type="text" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('last_names')} label="Apellidos" type="text" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('document')} label="N° Identificación" type="number" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('email')} label="Correo Electronico" type="email" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('phone')} label="N° Telefono" type="number" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('address')} label="Dirección" type="text" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <Controller
                        control={control}
                        name="department"
                        render={({ field }) => (
                            <Select {...field} options={departaments} className="w-11/12" onChange={filterCities} placeholder="Selecciona el departamento"/>
                        )}
                    />
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <Controller
                        control={control}
                        name="city"
                        render={({ field }) => (
                            <Select {...field} options={selectedCities} className="w-11/12" placeholder="Selecciona la ciudad"/>
                        )}
                    />
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('company')} label="Empresa" type="text" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('postal_code')} label="Codigo Postal" type="number" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('password')} label="Contraseña" type="password" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={12} className="flex justify-center pb-8">
                    <TextField {...register('confirm_password')} label="Confirma la contraseña" type="password" variant="outlined" className="w-11/12"/>
                </Grid>
                <Grid item lg={12} xs={12} sm={12} md={12} className="flex justify-center">
                    <Button onClick={handleSubmit(registerUser)} className={classes.button}>Registrarse</Button>
                </Grid>
            </Grid>
        </Container>
    );
}
export default FormRegister;