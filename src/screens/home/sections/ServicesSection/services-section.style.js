import styled from "styled-components";

const useServicesSectionStyles = () => {
    const StyledContainerSection = styled.section.attrs({
        className: 'services__section py-16'
    })`
      background-color: #182551;
    `;

    const StyledImage = styled.img.attrs({
        className: 'services__section__image'
    })`
        width: 100%;
    `;

    const StyledText = styled.h3.attrs({
        className: 'services__section__text text-white'
    })`
        font-size: 1.17em;
        font-weight: bold;
        font-family: Raleway,sans-serif;
    `;

    return {
        StyledContainerSection,
        StyledImage,
        StyledText,
    }
}

export default useServicesSectionStyles;