import React from "react";
import ReactDOM from 'react-dom';
import reportWebVitals from "./reportWebVitals";

// Components
import App from './app';

// Styles
import 'sweetalert2/dist/sweetalert2.css';

const root = document.getElementById('root');

ReactDOM.render(<App />, root);
reportWebVitals();