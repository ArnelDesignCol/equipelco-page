import styled from "styled-components";

const useCarouselInitialStyles = () => {
    const StyledImageCarousel = styled.img.attrs({
        className: 'image__carousel__initial'
    })`
        width: 100%;
        height: 35.2vw;
        
        @media (max-width: 1366px){
          height: 42.2vw;
        }
    `;

    return {
        StyledImageCarousel
    }
}

export default useCarouselInitialStyles