import useGeneralServices from "./general";
import useAuthServices from "./auth";
import useFooterServices from "./footer";
import useHomeServices from "./home";

const useServices = () => {
    return {
        useGeneralServices,
        useAuthServices,
        useFooterServices,
        useHomeServices,
    };
}

export default useServices;