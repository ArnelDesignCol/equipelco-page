import React from "react";
import useOffersSectionStyles from "./offers-section.style";
import {Container, Grid} from "@material-ui/core";
import useComponents from "../../../../components";
import useOffersSection from "./hook";
import _ from "lodash";

const OffersSection = () => {
    const {
        StyledContainer,
        StyledTitle,
        StyledImages
    } = useOffersSectionStyles();

    const {
        Carousel
    } = useComponents();

    const {
        offers
    } = useOffersSection();

    return (
        <StyledContainer>
            <Container>
                <Grid item lg={12}>
                    <StyledTitle>Nuestras Promociones</StyledTitle>
                </Grid>
                <Grid item lg={12}>
                    <Carousel
                        dots={false}
                        arrows={true}
                        slidesToShow={1}
                        slidesToScroll={1}
                        responsive={[
                            {
                                breakpoint: 1296,
                                settings: {
                                    dots: false,
                                    arrows: false
                                }
                            }
                        ]}
                    >
                        {
                            _.map(offers, (item, index) => (
                                <StyledImages src={item.image} key={index}/>
                            ))
                        }
                    </Carousel>
                </Grid>
            </Container>
        </StyledContainer>
    );
}

export default OffersSection;