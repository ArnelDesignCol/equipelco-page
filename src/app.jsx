import React, {Suspense} from 'react';
import useStore from "./store";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import Routes from "./routes";

// Styles
import './styles/style.css';

const App = () => {
    const { store, persist } = useStore();

    return (
        <Suspense fallback={<p>Cargando...!</p>}>
            <Provider store={store}>
                <PersistGate persistor={persist}>
                    <Routes/>
                </PersistGate>
            </Provider>
        </Suspense>
    );
}

export default App;