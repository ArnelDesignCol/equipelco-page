import styled from "styled-components";

const useHeaderStyle = () => {
    const StyledContainerHeader = styled.div.attrs({
        className: 'header'
    })`
      position: fixed;
      width: 100%;
      z-index: 11;
      @media (max-width: 1239px){
        position: relative;
      }
    `;

    const StyledContainerIconMenu = styled.div`
      @media screen and (min-width: 1240px) {
        display: none;
      }
    `;

    const StyledLogo = styled.img`
      width: 200px;
      padding: 20px 0px;
      margin-right: 40px;
    `;

    const StyledContainerMenu = styled.div`
      display: flex;
      justify-content: space-between !important;
      width: 100%;

      @media screen and (max-width: 1239px) {
        display: none;
      }
    `;

    const StyledAuth = styled.span`
      color: #000;
      margin-right: 20px;
      font-weight: 600;
    `;

    return {
        StyledContainerHeader,
        StyledContainerIconMenu,
        StyledLogo,
        StyledContainerMenu,
        StyledAuth
    }
}

export default useHeaderStyle;