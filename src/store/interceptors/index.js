import {useEffect} from "react";
import axios from 'axios';

const useInterceptors = () => {
    useEffect(() => {
        axios.defaults.baseURL = 'https://syswork.azurewebsites.net';
        const handleRequest = async(request) => {
            return await request;
        }

        const handleResponseError = (error) => {
            return error;
        }

        axios.interceptors.request.use(handleRequest);
        axios.interceptors.response.use((response) => {
            return response;
        }, async (error) =>{
            await handleResponseError(error);
            throw error;
        });
    }, []);
}

export default useInterceptors;