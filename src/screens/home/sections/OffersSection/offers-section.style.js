import styled from "styled-components";

const useOffersSectionStyles = () => {
    const StyledContainer = styled.section.attrs({
        className: 'offers__section py-16'
    })`
      background-color: #e6e6e6;
    `;

    const StyledTitle = styled.h2.attrs({
        className: 'title__section'
    })`
      color: #000;
      font-size: 1.5em;
      text-align: center;
      text-transform: uppercase;
      font-weight: 600;
      padding-bottom: 60px;
      font-family: Raleway, sans-serif;
    `;

    const StyledImages = styled.img.attrs({
        className: 'img__offers'
    })`
      width: 100%;
    `;

    return {
        StyledContainer,
        StyledTitle,
        StyledImages
    }
}

export default useOffersSectionStyles;