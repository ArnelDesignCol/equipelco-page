import axios from "axios";

const useHomeProviders = () => {
    const proGetCarousel = () => {
        return axios.get('/api/home/carousel-initial.json');
    }

    const proGetBrands = () => {
        return axios.get('/api/home/brands.json');
    }

    const proGetServicesSection = () => {
        return axios.get('/api/home/services-section.json');
    }

    const proGetOffers = () => {
        return axios.get('/api/home/offers.json');
    }

    const proGetProducts = () => {
        const Filtro = "SUCURSAL= 00004 "
        let request = "<PagXml>                                ";
             request += "   <CorXml>                             ";
             request += "       <TIPO>PARAMETRO</TIPO>            ";
             request += "       <CODIGO>0163</CODIGO>             ";
             request += "       <PAGINA>1</PAGINA>             ";
             request += "       <PAGINAS>20</PAGINAS>           ";
             request += "       <NOMBRE>PROCESO</NOMBRE>          ";
             request += "       <ORDEN>1</ORDEN>                  ";
             request += "       <VALOR><![CDATA[EMPRESA.CONTACTOS]]</VALOR>   ";
             request += "       <FILTRO><![CDATA[" + Filtro + "]]</FILTRO>     ";
             request += "   </CorXml>                                          ";
             request += "</PagXml>                                             ";

        return axios.post('https://syswork.azurewebsites.net/datos/webdatos.asmx/FiltroBusqueda', `Parametros=${request}`);
    }

    return {
        proGetCarousel,
        proGetBrands,
        proGetServicesSection,
        proGetOffers,
        proGetProducts
    }
}

export default useHomeProviders;