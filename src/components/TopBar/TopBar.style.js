import styled from "styled-components";

const useTopBarStyles = () => {
    const StyledContainer = styled.div.attrs({
        className: 'topbar'
    })`
      background-color: #182551;
      height: 64px;
      display: flex;
      align-items: center;
      color: #fff;

      @media screen and (max-width: 1239px) {
        display: none;
      }
    `;

    const StyledIcon = styled.i`
      font-size: 20px;
      margin-right: 10px;
    `;

    const StyledText = styled.span`
      font-size: 15px;
      margin-right: 20px;
      font-family: 'Ubuntu', sans-serif;
    `;

    const StyledImageSocialLinks = styled.img`
      width: 30px;
      margin-right: 20px;
    `;

    const StyledButtonOffers = styled.button`
      background-color: #eb5b25;
      color: #fff;
      font-family: "Raleway", sans-serif;
      position: fixed;
      top: 0%;
      border-bottom-right-radius: 20px;
      border-bottom-left-radius: 20px;
      border-color: transparent;
      padding: 10px;
      font-size: 13px;
    `;

    return {
        StyledContainer,
        StyledIcon,
        StyledText,
        StyledImageSocialLinks,
        StyledButtonOffers,
    }
}

export default useTopBarStyles;