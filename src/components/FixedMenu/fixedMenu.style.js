import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    container: {
        position: 'fixed',
        bottom: '0%',
        backgroundColor: '#182551',
        padding: '2% 5%',
        zIndex: 600,
        boxShadow: 'inset 0px 2px 4px -1px rgb(255 255 255 / 20%), inset 0px 4px 5px 0px rgb(255 255 255 / 14%), inset 0px 1px 10px 0px rgb(255 255 255 / 12%)',
        '@media (min-width: 1240px)': {
            display: 'none'
        },
        '@media (max-width: 540px)': {
            padding: '5% 5%'
        },

    },
    text: {
        color: '#fff',
        '@media (max-width: 591px)': {
            fontSize: '2vw'
        }
    },
    text_user: {
        color: '#fff',
        '@media (max-width: 591px)': {
            fontSize: '2vw'
        }
    },
    icon: {
        color: '#fff',
    },
});

export default useStyles;