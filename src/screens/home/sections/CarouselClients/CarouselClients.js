import React from 'react';
import useComponents from "../../../../components";
import {Container, Grid} from "@material-ui/core";
import useCarouselClientsStyles from "./carousel-clients.style";
import _ from "lodash";
import useCarouselClients from './hook';

const CarouselClients = () => {
    const {
        Carousel
    } = useComponents();

    const {
        StyledTitleSections,
        StyledContainerCarouselClients,
        StyledImageCarouselSections
    } = useCarouselClientsStyles();

    const {
        clients
    } = useCarouselClients
    return (
        <StyledContainerCarouselClients className="py-16">
            <Container>
                <Grid item lg={12}>
                    <StyledTitleSections>Nuestros Clientes</StyledTitleSections>
                </Grid>
                <Grid item lg={12}>
                    <Carousel
                        dots={true}
                        arrows={false}
                        slidesToShow={4}
                        slidesToScroll={4}
                        responsive={
                            [
                                {
                                    breakpoint: 500,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 2,
                                        infinite: true,
                                        dots: true,
                                        arrows: false
                                    }
                                },
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 3,
                                        infinite: true,
                                        dots: true,
                                        arrows: false
                                    }
                                }
                            ]
                        }
                    >
                        {
                            _.map(clients, (item, index) => (
                                <StyledImageCarouselSections src={item.image} key={index}/>
                            ))
                        }
                    </Carousel>
                </Grid>
            </Container>
        </StyledContainerCarouselClients>
    );
}

export default CarouselClients;