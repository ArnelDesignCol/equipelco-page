import React from 'react';
import useComponents from "../components";
import styled from "styled-components";

const MainLayout = props => {
    const {
        Header,
        Footer,
        FixedMenu,
    } = useComponents();

    const {children} = props;

    const StyledMain = styled.div`
      padding-top: 10rem;
      
      @media (max-width: 1239px) {
        padding-top: 0;
      }
    `;

    return (
        <React.Fragment>
            <Header/>
            <StyledMain>
                {children}
            </StyledMain>
            <FixedMenu/>
            <Footer/>
        </React.Fragment>
    );
}

export default MainLayout;