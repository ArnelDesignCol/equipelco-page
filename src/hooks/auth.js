import {useEffect, useState} from "react";
import {useForm} from "react-hook-form";
import useActions from "../store/actions";
import useSelectors from "../store/selectors";
import {useSelector} from "react-redux";
import _ from "lodash";
import Swal from "sweetalert2";
import {useHistory} from "react-router-dom";

const useAuth = () => {
    const history = useHistory();

    // Actions
    const {dispatch, useAuthActions} = useActions();
    const {actLogin, actRegisterUser, actGetDepartaments, actGetCities} = useAuthActions();

    // Selectors
    const {useAuthSelectors} = useSelectors();
    const {departamentsSelector, citiesSelector} = useAuthSelectors();
    const departamentsSelected = useSelector(departamentsSelector);
    const citiesSelected = useSelector(citiesSelector);

    // Handlers Effects
    const efectsHandlers = () => {
        dispatch(actGetDepartaments);
        dispatch(actGetCities);
    }

    // Effects
    useEffect(async () => {
        await efectsHandlers();
    }, []);

    // States
    const [value, setValue] = useState(1);
    const [departament, setDepartament] = useState({value: 0, label: ''});

    // Forms
    const {register, handleSubmit, control} = useForm();

    // Constants
    const cities = [];
    const departaments = [];

    _.map(departamentsSelected, (item, index) => {
        const obj = {
            value: item.CODIGO,
            label: item.NOMBRE
        };

        departaments.push(obj);
    });

    const handleChange = (event, newValue) => {
        setValue(newValue);
    }

    const allyProps = index => {
        return {
            id: `full-width-tab-${index}`,
            "aria-controls": `full-width-tabpanel-${index}`
        }
    }

    // Handlers
    const authenticateUser = data => {
        const {email, password} = data;
        dispatch(actLogin({email, password}));
    }

    const handleChangeDepartament = (data) => {
        try {
            const {value} = data;
            setDepartament(data);
            if(cities.length > 0) {
                cities.length = 0;
                citiesSelected.map((item) => {
                    if(item.DANE_DEP === value){
                        const obj = {
                            value: item.CODIGO,
                            label: item.NOMBRE
                        }

                        cities.push(obj);
                    }
                });
            }else{
                const precities = _.filter(citiesSelected, (item) => item.DANE_DEP === value);
                _.map(precities, (item) => {
                    const obj = {
                        value: item.CODIGO,
                        label: item.NOMBRE
                    }

                    cities.push(obj);
                })
                console.log(cities);
            }
        } catch (e) {
          console.log(e);
        }
    }

    const [selectedDepartment, setSelectedDepartment] = useState({});

    const registerUser = data => {
        data.department = selectedDepartment;
        if(data.password !== data.confirm_password){
            Swal.fire({
                icon: "error",
                text: "Las contraseñas no coinciden"
            })
        }else{
            dispatch(actRegisterUser(
                data.names,
                data.last_names,
                data.document,
                data.email,
                data.phone,
                data.address,
                data.department,
                data.city,
                data.company,
                data.postal_code,
                data.password,
                () => {
                    window.location.reload();
                }
            ));
        }

    }

    const [selectedCities, setSelectedCities] = useState([]);


    const filterCities = data => {
        setSelectedDepartment(data);
        const city = []
        if(selectedCities.length > 0){
            selectedCities.length = 0;
            _.map(citiesSelected, (item) => {
                if(item.DANE_DEP === data.value){
                    const obj = {
                        value: item.CODIGO,
                        label: item.NOMBRE
                    }
                    city.push(obj);
                }
            })
        }else{
            _.map(citiesSelected, (item) => {
                if(item.DANE_DEP === data.value){
                    const obj = {
                        value: item.CODIGO,
                        label: item.NOMBRE
                    }
                    city.push(obj);
                }
            })
        }
        setSelectedCities(city);
    }

    return {
        value,
        handleChange,
        allyProps,
        register,
        handleSubmit,
        authenticateUser,
        registerUser,
        departaments,
        departament,
        cities,
        handleChangeDepartament,
        control,
        isLogged: false,
        filterCities,
        selectedCities
    }
}

export default useAuth;