import React from 'react';
import useHeaderStyle from "./Header.style";
import useComponents from "../index";
import {AppBar, Container, IconButton, Toolbar} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import {Link} from "react-router-dom";
import useAuth from "../../hooks/auth";

const Header = () => {
    const {
        StyledContainerHeader,
        StyledContainerIconMenu,
        StyledLogo,
        StyledContainerMenu,
        StyledAuth,
    } = useHeaderStyle();

    const {
        TopBar
    } = useComponents();

    const {isLogged} = useAuth();

    return (
        <StyledContainerHeader>
            <TopBar/>
            <AppBar position="relative" color="inherit">
                <Container>
                    <Toolbar>
                        <StyledContainerIconMenu>
                            <IconButton edge="start" className="" aria-label="menu">
                                <MenuIcon/>
                            </IconButton>
                        </StyledContainerIconMenu>
                        <Link to="/">
                            <StyledLogo src="/images/general/logo/Logo.png" alt="equipelco" title="equipelco"/>
                        </Link>
                        <StyledContainerMenu>
                            <div>Hola</div>
                            <div>
                                {
                                    isLogged ?
                                        <Link to="/auth">
                                            <StyledAuth>Ingresa</StyledAuth>
                                        </Link> :
                                        <Link to="/auth">
                                            <StyledAuth>Ingresa</StyledAuth>
                                        </Link>
                                }

                            </div>
                        </StyledContainerMenu>
                    </Toolbar>
                </Container>
            </AppBar>
        </StyledContainerHeader>
    );
}

export default Header;