import React from 'react';
import {BrowserRouter as Router, Switch, Route as DefaultRoute} from "react-router-dom";
import useScreens from "../screens";
import MainLayout from "../layout/MainLayout";

const Route = ({
    component: Component,
    layout: Layout = Component.Layout || MainLayout,
    propsLayout = Component.props,
    ...rest
}) => {
    return (
        <DefaultRoute {...rest} render={(props) => (
            <Layout {...propsLayout}>
                <Component {...props}/>
            </Layout>
        )}/>
    );
}

const Routes = () => {
    const {
        Home,
        Auth
    } = useScreens();

    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/auth" component={Auth}/>
            </Switch>
        </Router>
    );
}

export default Routes;