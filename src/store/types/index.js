// General types
export const GET_SOCIAL_LINKS = "GET_SOCIAL_LINKS";

// Auth Types
export const LOGIN = "LOGIN";
export const GET_CITIES = "GET_CITIES";
export const GET_DEPARTAMENTS = "GET_DEPARTAMENTS";
export const REGISTER_USER = "REGISTER_USER";

// Footer Types
export const GET_PAYMENT_METHODS = "GET_PAYMENT_METHODS";
export const GET_CONTACT_DETAIL = "GET_CONTACT_DETAIL";
export const GET_SERVICES_DETAIL = "GET_SERVICES_DETAIL";

// Home Types
export const GET_CAROUSEL = "GET_CAROUSEL";
export const GET_BRANDS = "GET_BRANDS";
export const GET_SERVICES_SECTION = "GET_SERVICES_SECTION";
export const GET_OFFERS = "GET_OFFERS";
export const GET_PRODUCTS_HOME = "GET_PRODUCTS_HOME";