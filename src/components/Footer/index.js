import React from 'react';
import useFooterStyles from "./footer.style";
import useFooter from '../../hooks/footer';
import {Container, Grid, List, ListItem, ListItemText} from "@material-ui/core";
import _ from "lodash";
import {Link} from "react-router-dom";

const Footer = () => {
    const {
        StyledFooter,
        StyledTitle,
        StyledPaymentMethod,
        StyledIcon,
        StyledDetail,
        useStyles,
    } = useFooterStyles();

    const {
        paymentMethods,
        contactDetail,
        serviceDetail,
        hover,
        handlerHover
    } = useFooter();
    const classes = useStyles();

    return (
        <StyledFooter>
            <Container>
                <Grid container className={classes.rowPaymentMethods}>
                    <Grid item lg={12}>
                        <StyledTitle>Medios de pago</StyledTitle>
                    </Grid>
                    <Grid item lg={12}>
                        <Grid container>
                            {
                                paymentMethods && _.map(paymentMethods, (item, index) => (
                                    <Grid item lg={1} xs={2} key={index}>
                                        {
                                            hover.id === item.id && hover.hover ? (
                                                <StyledPaymentMethod src={item.imgColor} onMouseLeave={() => handlerHover('leave')}/>
                                            ) : (
                                                <StyledPaymentMethod src={item.imgGrey} onMouseOver={() => handlerHover('over', item.id)}/>
                                            )
                                        }
                                    </Grid>
                                ))
                            }
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container className="pt-10">
                    <Grid item lg={4} className={classes.contactClass}>
                        <Grid item lg={12}>
                            <StyledTitle>Contactenos</StyledTitle>
                        </Grid>
                        <Grid item lg={12} className="pt-8">
                            <Grid container>
                                {
                                    _.map(contactDetail, (contact, index) => (
                                        <React.Fragment key={index}>
                                            <Grid item lg={2} xs={2}  className="flex content-center py-6 justify-center">
                                                <StyledIcon className={contact.icon}/>
                                            </Grid>
                                            <Grid item lg={10} xs={10} className="content-center flex" style={{ flexWrap: 'wrap' }}>
                                                <Grid item lg={12} xs={12}>
                                                    <StyledDetail>{contact.title}</StyledDetail>
                                                </Grid>
                                                <Grid item lg={12} xs={12}>
                                                    <StyledDetail>{contact.text}</StyledDetail>
                                                </Grid>
                                            </Grid>
                                        </React.Fragment>
                                    ))
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item lg={4} md={6} sm={12} xs={12} className={classes.servicesClass}>
                        <Grid item lg={12}>
                            <StyledTitle>Servicio Al Cliente</StyledTitle>
                        </Grid>
                        <Grid item lg={12}>
                            <List dense>
                                {
                                    serviceDetail && _.map(serviceDetail, (item, index) => (
                                        <ListItem key={index}>
                                            <Link to={item.url} className={classes.textList}>
                                                <ListItemText>
                                                    <h3 className={classes.textList}>{item.name}</h3>
                                                </ListItemText>
                                            </Link>
                                        </ListItem>
                                    ))
                                }
                            </List>
                        </Grid>
                    </Grid>
                    <Grid item lg={4} md={6} sm={12} xs={12} className={classes.usersClass}>
                        <Grid item lg={12}>
                            <StyledTitle>Mi cuenta</StyledTitle>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
        </StyledFooter>
    );
}

export default Footer;