import useCreateSelector from "../createSelector";
import _ from "lodash";

const useAuthSelectors = () => {
    const {createSelector} = useCreateSelector();

    const authCredentials = createSelector(
        state => state.authCredencials,
        authCredencials => {
            try {
                return authCredencials.login;
            } catch(err){
                console.log(err);
            }
        }
    );

    const departamentsSelector = createSelector(
        state => state.departaments,
        departaments => {
            try {
                const { entities, results } = departaments;
                return _.map(results.departaments, (m) => entities.departament[m]);
            } catch (e) {
                console.log(e);
            }
        }
    );

    const citiesSelector = createSelector(
        state => state.cities,
        cities => {
            try {
                const { entities, results } = cities;
                return _.map(results.cities, (m) => entities.city[m]);
            } catch (e) {
                console.log(e);
            }
        }
    );

    return {
        authCredentials,
        departamentsSelector,
        citiesSelector
    }
}

export default useAuthSelectors;