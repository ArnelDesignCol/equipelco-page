import React from 'react';
import {Button, Container, Grid, TextField, Typography} from "@material-ui/core";
import useComponents from "../../components";
import useAuth from "../../hooks/auth";
import useAuthStyles from "./Auth.style";
import Select from 'react-select';

const Auth = () => {
    const {NavigationTabs, NavigationTab, FormRegister} = useComponents();

    const {
        allyProps,
        value,
        handleChange,
        register,
        handleSubmit,
        authenticateUser,
        registerUser,
        departaments,
        control,
        departament,
        handleChangeDepartament,
        cities
    } = useAuth();

    const {
        StyledSectionBody,
        StyledNavSection,
        StyledAuth,
        StyledContent,
        StyledFormLogin,
        classesLogin,
        classesRegister
    } = useAuthStyles();

    const StylesLoginForm = classesLogin();
    const classesRegisterStyles = classesRegister();

    return (
        <StyledAuth>
            <StyledContent>
                <NavigationTabs
                    value={value}
                    onChange={handleChange}
                    aria-label="Tabs Auth"
                    variant="standard"
                >
                    <NavigationTab label="SOY USUARIO NUEVO" {...allyProps(value)}/>
                    <NavigationTab label="YA TENGO CUENTA" {...allyProps(value)}/>
                </NavigationTabs>
                <StyledSectionBody>
                    <StyledNavSection className={value === 0 && "active"}>
                        <Typography variant="h6" className="text-center px-6 mb-4 mt-6 py-6">REGISTRATE E INGRESA A UN
                            MUNDO DE PRODUCTOS Y PROMOCIONES...!</Typography>
                        <FormRegister/>
                    </StyledNavSection>
                    <StyledNavSection className={value === 1 && "active"}>
                        <Typography variant="h6" className="text-center px-6 mb-4 mt-6 py-6">INGRESAR A NUESTRO
                            ECOMMERCE Y COMPREMOS JUNTOS UN UNIVERSO DE PRODUCTOS, Y CONOCENOS PARA QUE COMPRES
                            SEGURO</Typography>
                        <StyledFormLogin onSubmit={handleSubmit(authenticateUser)}>
                            <TextField {...register('email')} type="email" required label="Correo Electronico"
                                       variant="outlined" className={StylesLoginForm.input}/>
                            <TextField {...register('password')} type="password" required label="Contraseña"
                                       variant="outlined" className={StylesLoginForm.input}/>
                            <Grid item lg={12} className="flex justify-center py-8">
                                <Button className={StylesLoginForm.button} type="submit">Ingresar</Button>
                            </Grid>
                        </StyledFormLogin>
                    </StyledNavSection>
                </StyledSectionBody>
            </StyledContent>
        </StyledAuth>
    );
}

export default Auth;