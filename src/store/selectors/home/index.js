import useCreateSelector from "../createSelector";
import _ from "lodash";

const useHomeSelectors = () => {
    const {createSelector} = useCreateSelector();

    const carouselSelector = createSelector(
        state => state.carousel,
        carousel => {
            try {
                const {entities, results} = carousel;
                return _.map(results.carousels, (m) => entities.carousel[m]);
            } catch (e) {
                return [];
            }
        }
    );

    const brandsSelector = createSelector(
        state => state.brands,
        brands => {
            try {
                const {entities, results} = brands;
                return _.map(results.brands, (m) => entities.brand[m]);
            } catch (e) {
                return [];
            }
        }
    );

    const servicesSelector = createSelector(
        state => state.servicesSection,
        servicesSection => {
            try {
                const {entities, results} = servicesSection;
                return _.map(results.services, (m) => entities.service[m]);
            } catch (e) {
                return [];
            }
        }
    );

    const offersSelector = createSelector(
        state => state.offers,
        offers => {
            try {
                const {entities, results} = offers;
                return _.map(results.offers, (m) => entities.offer[m]);
            } catch (e) {
                return [];
            }
        }
    );

    const productsSelector = createSelector(
        state => state.productsHome,
        productsHome => {
            try {
                const {entities, results} = productsHome;
                return _.map(results.products, (m) => entities.product[m]);
            } catch (e) {
                return [];
            }
        }
    );

    return {
        carouselSelector,
        brandsSelector,
        servicesSelector,
        offersSelector,
        productsSelector
    }
}

export default useHomeSelectors;