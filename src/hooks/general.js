import useActions from "../store/actions";
import useSelectors from "../store/selectors";
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {number, string} from 'prop-types';

const useGeneral = () => {
    // Actions
    const {dispatch, useGeneralActions} = useActions();
    const {actGetSocialLinks} = useGeneralActions();

    // Selectors
    const {useGeneralSelectors} = useSelectors();
    const {socialLinksSelector} = useGeneralSelectors();
    const socialLinks = useSelector(socialLinksSelector);

    // States
    const [hover, setHover] = useState({id: 0, hover: false});

    // Effects
    useEffect(() => {
        dispatch(actGetSocialLinks);
    }, [dispatch, actGetSocialLinks]);

    // handlers
    const handleHover = (type = string.isRequired, id = number ) => {
        if (type !== 'over') {
            setHover({id: 0, hover: false});
        } else {
            setHover({id, hover: true});
        }
    }

    return {
        socialLinks,
        hover,
        handleHover,
    }
}

export default useGeneral;