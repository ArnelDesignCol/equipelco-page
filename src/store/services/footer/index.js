import useProviders from "../../providers";

const useFooterServices = () => {
    const { useFooterProviders } = useProviders();
    const { proGetPaymentMethods, proGetContactDetail, proGetServicesDetail } = useFooterProviders();
    
    const serGetPaymentMethods = () => {
        return new Promise(async(resolve, reject) => {
            try {
                resolve(await proGetPaymentMethods());
            } catch (e) {
                reject(e);
            }
        });
    }

    const serGetContactDetail = () => {
        return new Promise(async(resolve, reject) => {
            try {
                resolve(await proGetContactDetail());
            } catch (e) {
                reject(e);
            }
        });
    }

    const serGetServicesDetail = () => {
        return new Promise(async(resolve, reject) => {
            try {
                resolve(await proGetServicesDetail());
            } catch (e) {
                reject(e);
            }
        });
    }

    return {
        serGetPaymentMethods,
        serGetContactDetail,
        serGetServicesDetail,
    }
}

export default useFooterServices;