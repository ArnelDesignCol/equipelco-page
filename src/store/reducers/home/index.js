import createReducer from "../createReducer";
import {GET_BRANDS, GET_CAROUSEL, GET_OFFERS, GET_PRODUCTS_HOME, GET_SERVICES_SECTION} from "../../types";

const useHomeReducers = () => {
    const carousel = createReducer({
        entities: {},
        results: []
    }, {
        [GET_CAROUSEL](state, action) {
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    const brands = createReducer({
        entities: {},
        results: []
    }, {
        [GET_BRANDS](state, action) {
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    const servicesSection = createReducer({
        entities: {},
        results: []
    }, {
        [GET_SERVICES_SECTION](state, action) {
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    const offers = createReducer({
        entities: {},
        results: []
    }, {
        [GET_OFFERS](state, action) {
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    const productsHome = createReducer({
        entities: {},
        results: []
    }, {
        [GET_PRODUCTS_HOME](state, action) {
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    return {
        carousel,
        brands,
        servicesSection,
        offers,
        productsHome
    }
}

export default useHomeReducers;