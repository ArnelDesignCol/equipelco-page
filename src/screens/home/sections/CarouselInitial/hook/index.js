import useActions from "../../../../../store/actions";
import useSelectors from "../../../../../store/selectors";
import {useSelector} from "react-redux";
import {useEffect} from "react";

const useCarouselInitial = () => {
    // Actions
    const {dispatch, useHomeActions} = useActions();
    const {actGetCarousel} = useHomeActions();

    // Selectors
    const {useHomeSelectors} = useSelectors();
    const {carouselSelector} = useHomeSelectors();
    const carousel = useSelector(carouselSelector);

    useEffect(() => {
        dispatch(actGetCarousel);
    }, [dispatch])

    return {
        carousel
    }
}

export default useCarouselInitial;