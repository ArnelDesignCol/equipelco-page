import React from 'react';
import useInputStyles from "./Input.style";

const {
    StyledInput
} = useInputStyles();

const InputComponent = (controlInput) => {
    return (
        <div className="StyledWrapperInput">
            <div>
                {
                    label && (
                        <label
                            htmlFor={id}
                            className={required ? `block text-base font-customNormal text-white mb-3` : `block text-base font-customNormal text-white mb-3`}
                        >
                            {label}
                        </label>
                    )
                }
                <StyledInput
                    type={type}
                    id={id}
                    dark={dark}
                    placeholder={placeholder}
                    
                />
            </div>
        </div>
    );
}

const Input = props => {


    return (
        <Controller
            name={name}
            control={control}
            rules={rules}
            render={InputComponent}
            defaultValue={defaultValue}
        />
    );
}

export default Input;