import React from 'react';
import PropTypes from 'prop-types';
import useTabPanelStyles from "./TabPanel.style";
import _ from "lodash";
import useComponents from "../index";

const NavigationTabPanel = props => {
    const {StyledTabPanel} = useTabPanelStyles();

    const {className, links} = props;

    const panelLinks = links?.filter(link => link.navPanel);

    const {NavigationTab} = useComponents();

    return (
        <StyledTabPanel className={className}>
            {
                _.map(panelLinks, (link, index) => (
                    <NavigationTab
                        label={link.aliasPanel}
                        to={link.to}
                        selected={link.routes?.some(r => window.location.pathname === r)}
                        disabled={link.disabled}
                        onClick={link.onClick}
                        icon={link.icon}
                    />
                ))
            }
        </StyledTabPanel>
    );
}

NavigationTabPanel.propTypes = {
    className: PropTypes.string,
    url: PropTypes.string,
    logoutCallback: PropTypes.func,
    titleModal: PropTypes.string,
}

NavigationTabPanel.defaultProps = {
    className: 'NavigationPanel',
    url: '',
    titleModal: "Cerrar Modal",
}

export default NavigationTabPanel;