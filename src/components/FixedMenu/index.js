import React from "react";
import {Grid, Menu, MenuItem} from "@material-ui/core";
import useStyles from "./fixedMenu.style";
import useFixedMenu from "./hook";
import Submenu from "../submenus-container";
import {Link} from "react-router-dom";

const FixedMenu = () => {
    const classes = useStyles();

    const { login, anchorEl, open, openMenu, handleClose, handleLogout } = useFixedMenu();

    return (
        <Grid container className={classes.container}>
            <Grid item md={3} sm={3} xs={3}>
                <Grid item md={12} className="flex justify-center">
                    <i className={"fas fa-store-alt text-white"}></i>
                </Grid>
                <Grid item md={12} className="flex justify-center">
                    <span className={classes.text}>Tienda</span>
                </Grid>
            </Grid>
            <Grid item md={3} sm={3} xs={3}>
                <Grid item md={12} className="flex justify-center">
                    <i className={"fas fa-store-alt text-white"}></i>
                </Grid>
                <Grid item md={12} className="flex justify-center">
                    <span className={classes.text}>Tienda</span>
                </Grid>
            </Grid>
            <Grid item md={3} sm={3} xs={3}>
                <Grid item md={12} className="flex justify-center">
                    <i className={"fas fa-store-alt text-white"}></i>
                </Grid>
                <Grid item md={12} className="flex justify-center">
                    <span className={classes.text}>Tienda</span>
                </Grid>
            </Grid>
            <Grid item md={3} sm={3} xs={3} >
                {
                    login.NOMBRES ? (
                        <React.Fragment>
                            <Grid item md={12} className="flex justify-center" onClick={(e) => openMenu(e)}>
                                <i className="far fa-user text-white"></i>
                            </Grid>
                            <Grid item md={12} className="flex justify-center" onClick={(e) => openMenu(e)}>
                                <span className={classes.text_user}>{`${login.NOMBRES}`}</span>
                            </Grid>
                            <Submenu open={open} type="fixedMenu" logout={handleLogout}/>
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            <Grid item md={12} className="flex justify-center">
                                <i className="far fa-user text-white"></i>
                            </Grid>
                            <Grid item md={12} className="flex justify-center">
                                <Link to="/auth">
                                    <span className={classes.text_user}>Ingresar</span>
                                </Link>
                            </Grid>
                        </React.Fragment>
                    )
                }
            </Grid>

        </Grid>
    );
}

export default FixedMenu;