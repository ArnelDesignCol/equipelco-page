import React from 'react';
import useHomeSections from "./sections";
import ServicesSection from "./sections/ServicesSection";

const Home = () => {
    const {
        CarouselInitial,
        CarouselBrands,
        CarouselClients,
        ServicesSection,
        OffersSection,
        ProductCarousel
    } = useHomeSections();

    return (
        <React.Fragment>
            <CarouselInitial/>
            <CarouselBrands/>
            <ServicesSection/>
            <OffersSection/>
            <ProductCarousel/>
            <CarouselClients/>
        </React.Fragment>
    );
}

export default Home;