import createReducer from "../createReducer";
import {GET_CITIES, GET_DEPARTAMENTS, LOGIN} from "../../types";

const useAuthReducers = () => {
    const authCredencials = createReducer({
        login: {}
    }, {
        [LOGIN](state, action){
            return {
                ...state,
                login: action.payload
            }
        }
    });

    const departaments = createReducer({
        entities: {},
        results: []
    }, {
        [GET_DEPARTAMENTS](state, action){
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    const cities = createReducer({
        entities: {},
        results: []
    }, {
        [GET_CITIES](state, action){
            return {
                ...state,
                entities: action.payload.entities,
                results: action.payload.result
            }
        }
    });

    return {
        authCredencials,
        departaments,
        cities
    }
}

export default useAuthReducers;