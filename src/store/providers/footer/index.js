import axios from "axios";

const useFooterProviders = () => {
    const proGetPaymentMethods = () => {
        return axios.get('/api/footer/payment-methods.json');
    }

    const proGetContactDetail = () => {
        return axios.get('/api/footer/contact-detail.json');
    }

    const proGetServicesDetail = () => {
        return axios.get('/api/footer/services-detail.json');
    }

    return {
        proGetPaymentMethods,
        proGetContactDetail,
        proGetServicesDetail,
    }
}

export default useFooterProviders;