import React from 'react';

// Components
import Home from './home';
import Auth from "./auth";

const useScreens = () => {
    return {
        Home,
        Auth,
    };
}

export default useScreens;