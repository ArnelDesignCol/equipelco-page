import useGeneralSelectors from "./general";
import useAuthSelectors from "./auth";
import useFooterSelectors from "./footer";
import useHomeSelectors from "./home";

const useSelectors = () => {
    return {
        useGeneralSelectors,
        useAuthSelectors,
        useFooterSelectors,
        useHomeSelectors,
    };
}

export default useSelectors;