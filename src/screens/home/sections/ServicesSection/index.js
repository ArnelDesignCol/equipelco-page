import React from "react";
import useServicesSectionStyles from "./services-section.style";
import {Container, Grid} from "@material-ui/core";
import useServicesSection from "./hook";
import _ from "lodash";

const ServicesSection = () => {
    const {
        StyledContainerSection,
        StyledImage,
        StyledText,
    } = useServicesSectionStyles();

    const {
        services
    } = useServicesSection();

    return (
        <StyledContainerSection>
            <Container maxWidth={"md"}>
                <Grid container>
                    {
                        _.map(services, (item, index) => (
                            <Grid item lg={3} md={3} sm={3} xs={6} key={index}>
                                {
                                    item.image !== undefined ? (
                                        <StyledImage src={item.image}/>
                                    ) : (
                                        <StyledText>{item.text}</StyledText>
                                    )
                                }
                            </Grid>
                        ))
                    }
                </Grid>
            </Container>
        </StyledContainerSection>
    );
}
export default ServicesSection;