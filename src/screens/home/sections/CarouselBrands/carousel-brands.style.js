import styled from "styled-components";

const useCarouselBrandsStyles = () => {
    const StyledContainerCarouselBrands = styled.section.attrs({
        className: 'carousel-brands'
    })`
      background-color: #e6e6e6;
    `;

    const StyledImageCarouselBrands = styled.img.attrs({
        className: 'image__carousel__brands'
    })`
      width: 90%;
      padding: 0px 10px;
    `;

    return {
        StyledContainerCarouselBrands,
        StyledImageCarouselBrands
    }
}

export default useCarouselBrandsStyles;