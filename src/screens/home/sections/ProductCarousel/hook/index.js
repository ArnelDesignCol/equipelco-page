import React, {useEffect} from "react";
import useActions from "../../../../../store/actions";
import useSelectors from "../../../../../store/selectors";
import {useSelector} from "react-redux";

const useProductsCarousel = () => {
    // Actions
    const {dispatch, useHomeActions} = useActions();
    const {actGetProducts} = useHomeActions();

    // Selectors
    const {useHomeSelectors} = useSelectors();
    const {productsSelector} = useHomeSelectors();
    const products = useSelector(productsSelector);

    // Effects
    useEffect(() => {
        dispatch(actGetProducts);
    }, [dispatch]);

    console.log(products);
  return {
    products
  }
}

export default useProductsCarousel;