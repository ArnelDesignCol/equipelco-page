import CarouselClients from "./CarouselClients/CarouselClients";
import CarouselBrands from "./CarouselBrands/CarouselBrands";
import CarouselInitial from "./CarouselInitial";
import ServicesSection from "./ServicesSection";
import OffersSection from "./OffersSection";
import ProductCarousel from "./ProductCarousel";

const useHomeSections = () => {
    return {
        CarouselClients,
        CarouselBrands,
        CarouselInitial,
        ServicesSection,
        OffersSection,
        ProductCarousel
    }
}

export default useHomeSections;