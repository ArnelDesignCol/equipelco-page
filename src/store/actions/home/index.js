import useServices from "../../services";
import {normalize, schema} from "normalizr";
import {GET_BRANDS, GET_CAROUSEL, GET_OFFERS, GET_PRODUCTS_HOME, GET_SERVICES_SECTION} from "../../types";

const useHomeActions = () => {
    const {useHomeServices} = useServices();
    const {serGetCarousel, serGetBrands, serGetServicesSection, serGetOffers, serGetProducts} = useHomeServices();

    const actGetCarousel = async dispatch => {
        try {
            const res = await serGetCarousel();
            const {data} = res;

            const schemaData = {
                carousels: data
            }

            const carousel = new schema.Entity('carousel');

            const schemaStructure = {
                carousels: [carousel]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_CAROUSEL,
                payload: {
                    entities,
                    result
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    const actGetBrands = async dispatch => {
        try {
            const res = await serGetBrands();
            const {data} = res;

            const schemaData = {
                brands: data
            }

            const brand = new schema.Entity('brand');

            const schemaStructure = {
                brands: [brand]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_BRANDS,
                payload: {
                    entities,
                    result
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    const actGetServicesSection = async dispatch => {
        try {
            const res = await serGetServicesSection();
            const {data} = res;

            const schemaData = {
                services: data
            }

            const service = new schema.Entity('service');

            const schemaStructure = {
                services: [service]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_SERVICES_SECTION,
                payload: {
                    entities,
                    result
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    const actGetOffers = async dispatch => {
        try {
            const res = await serGetOffers();
            const {data} = res;

            const schemaData = {
                offers: data
            }

            const offer = new schema.Entity('offer');

            const schemaStructure = {
                offers: [offer]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_OFFERS,
                payload: {
                    entities,
                    result
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    const actGetProducts = async dispatch => {
        try {
            const res = await serGetProducts();
            const {data} = res;

            const subCadenaInicio = data.substring(90);
            const newValue = JSON.parse(subCadenaInicio.replace("}</string>", ""));

            const schemaData = {
                products: newValue.Datos
            }

            const product = new schema.Entity('product');

            const schemaStructure = {
                products: [product]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_PRODUCTS_HOME,
                payload: {
                    entities,
                    result
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    return {
        actGetCarousel,
        actGetBrands,
        actGetServicesSection,
        actGetOffers,
        actGetProducts,
    }
}

export default useHomeActions;