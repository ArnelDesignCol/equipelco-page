import React from 'react';
import useTopBarStyles from "./TopBar.style";
import {Container, Grid} from "@material-ui/core";
import useGeneral from "../../hooks/general";
import _ from "lodash";

const TopBar = () => {
    const {
        StyledContainer,
        StyledIcon,
        StyledText,
        StyledImageSocialLinks,
        StyledButtonOffers
    } = useTopBarStyles();

    const {socialLinks, hover, handleHover} = useGeneral();

    return (
        <StyledContainer>
            <Container>
                <Grid container>
                    <Grid item md={7}>
                        <Grid container className="items-center">
                            <StyledIcon className="fab fa-whatsapp"/>
                            <StyledText>+57 317 443 1072</StyledText>
                            <StyledIcon className="fas fa-phone"/>
                            <StyledText>Pbx: +57 (2) 487 60 64</StyledText>
                            <StyledIcon className="far fa-envelope"/>
                            <StyledText>Correo: mercadeodigital@equipelco.com</StyledText>
                        </Grid>
                    </Grid>
                    <Grid item md={2} className="flex justify-center">
                        <StyledButtonOffers>
                            <i className="fas fa-tags" style={{marginRight: 5}}/>
                             Ofertas

                        </StyledButtonOffers>
                    </Grid>
                    <Grid item md={3}>
                        <Grid container className="flex justify-end">
                            {
                                _.map(socialLinks, (item, key) => (
                                    <div key={key}>
                                        {
                                            hover.id === item.id && hover.hover === true ?
                                                <StyledImageSocialLinks src={item.image}
                                                                        onMouseLeave={() => handleHover('leave')}/> :
                                                <StyledImageSocialLinks src={item.image_grey}
                                                                        onMouseOver={() => handleHover('over', item.id)}/>
                                        }
                                    </div>
                                ))
                            }
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
        </StyledContainer>
    );
}

export default TopBar;