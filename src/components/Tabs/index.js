import React, {useState} from 'react';
import useTabsStyle from "./Tabs.style";
import {Tabs} from '@material-ui/core';
import PropTypes from 'prop-types';

const NavigationTabs = props => {
    const {StyledTabs} = useTabsStyle();
    const {children, value, handleChange, variant, className, label, ...other} = props;
    return (
        <StyledTabs
            as={Tabs}
            value={value}
            onChange={handleChange}
            variant={variant}
            className={className}
            aria-label={label}
            {...other}
        >
            {children}
        </StyledTabs>
    );
}

NavigationTabs.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
    children: PropTypes.node,
    value: PropTypes.any.isRequired,
    handleChange: PropTypes.func,
    variant: PropTypes.oneOf(["fullWidth", "standard"])
};

NavigationTabs.defaultProps = {
    label: "full width tabs example",
    className: "Tabs",
    variant: "fullWidth",
    handleChange: null,
    children: null,
}

export default NavigationTabs;