import styled from "styled-components";
import tw from "twin.macro";

const useTabPanelStyles = () => {
    const StyledTabPanel = styled.div.attrs({
        className: 'TabPanel'
    })`
      ${tw`fixed flex overflow-hidden lg:hidden bottom-0 w-full bg-primary justify-around py-2 border-t z-50`}
    `;

    const StyledPanelLink = styled.div.attrs({
        className: 'PanelLink'
    })`
      ${tw``}
      .StyledTab {
        ${tw` p-3`}
        
        p {
          ${tw` text-sm border-none p-0`}
        }
      }
    `;

    return {
        StyledTabPanel,
        StyledPanelLink
    };
}

export default useTabPanelStyles;