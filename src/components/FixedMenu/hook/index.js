import useSelectors from "../../../store/selectors";
import {useSelector} from "react-redux";
import {useState} from "react";
import useActions from "../../../store/actions";

const useFixedMenu = () => {
    // Actions
    const { dispatch, useAuthActions } = useActions();
    const { actLogout } = useAuthActions();

    // Selectors
    const { useAuthSelectors } = useSelectors();
    const { authCredentials } = useAuthSelectors();
    const login = useSelector(authCredentials);

    // States
    const [anchorEl, setAnchorEl] = useState("top");
    const [open, setOpen] = useState(false);

    // Handlers
    const openMenu = (event) => {
        setAnchorEl(event.currentTarget);
        if(open) {
            setOpen(false);
        }else{
            setOpen(true);
        }
    }

    const handleLogout = () => {
        dispatch(actLogout);
        setOpen(false);
    }

    return {
        login,
        anchorEl,
        open,
        openMenu,
        handleLogout
    }
}

export default useFixedMenu;