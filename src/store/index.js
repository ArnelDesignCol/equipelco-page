import useCombineReducers from "./reducers";
import storage from 'redux-persist/lib/storage';
import {persistReducer, persistStore} from "redux-persist";
import thunk from 'redux-thunk';
import {applyMiddleware, compose, createStore} from "@reduxjs/toolkit";

const useStore = () => {
    const initialState = {};

    let middleware = [];

    const rootReducers = useCombineReducers();

    const persistConfig = {
        key: 'root',
        storage: storage,
        blacklist: []
    }

    const persistReduce = persistReducer(persistConfig, rootReducers);

    if(process.env.NODE_ENV === 'development'){
        const reduxInmutableStateInvariant = require('redux-immutable-state-invariant').default();
        middleware = [...middleware, reduxInmutableStateInvariant, thunk]
    }else{
        middleware = [...middleware, thunk];
    }

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    let store = createStore(
        persistReduce,
        initialState,
        composeEnhancers(applyMiddleware(...middleware))
    );

    const persist = persistStore(store);

    return {
        store,
        persist
    }
}

export default useStore;