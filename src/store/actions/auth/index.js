import PropTypes from "prop-types";
import useServices from "../../services";
import {GET_CITIES, GET_DEPARTAMENTS, LOGIN} from "../../types";
import {normalize, schema} from "normalizr";
import Swal from "sweetalert2";
import {useHistory} from "react-router-dom";

const useAuthActions = () => {
    const {useAuthServices} = useServices();
    const {serLogin, serRegisterUser, serGetDepartaments, serGetCities} = useAuthServices();
    const history = useHistory();

    const actLogin = ({email, password}) => async dispatch => {
        try {
            const res = await serLogin({email, password});
            const {data} = res;

            const subCadenaInicio = data.substring(90);
            const newValue = JSON.parse(subCadenaInicio.replace("}</string>", ""));
            const datas = newValue.Datos;

            if(newValue.Estado.Registros === "0"){
                Swal.fire({
                    icon: "error",
                    text: "Los datos ingresados son incorrectos, revisalos e intenta nuevamente."
                });
            }else{
                dispatch({
                    type: LOGIN,
                    payload: datas
                });

                Swal.fire({
                   icon: "success",
                   text: "Bienvenido otra vez!"
                }).then((res) => {
                    if(res.isConfirmed){
                        history.push('/');
                    }
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    actLogin.propTypes = {
        email: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired
    }

    const actRegisterUser = (
        names,
        last_names,
        document,
        email,
        phone,
        address,
        department,
        city,
        company,
        postal_code,
        password,
        onSuccess
    ) => async dispatch => {
        try {
            const res = await serRegisterUser(
                names,
                last_names,
                document,
                email,
                phone,
                address,
                department,
                city,
                company,
                postal_code,
                password
            );
            const {data} = res;

            const subCadenaInicio = data.substring(90);
            const newValue = JSON.parse(subCadenaInicio.replace("}</string>", ""));
            const datas = newValue.Datos;

            onSuccess();

            console.log(data);
        } catch (e) {
            console.log(e);
        }
    }

    serRegisterUser.propTypes = {
        names: PropTypes.string.isRequired,
        last_names: PropTypes.string.isRequired,
        document: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        department: PropTypes.object.isRequired,
        city: PropTypes.object.isRequired,
        company: PropTypes.string.isRequired,
        postal_code: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired
    }

    const actGetDepartaments = async dispatch => {
        try {
            const res = await serGetDepartaments();
            const {data} = res;

            const subCadenaInicio = data.substring(90);
            const newValue = JSON.parse(subCadenaInicio.replace("}</string>", ""));
            const departaments = newValue.Datos;

            const schemaData = {
                departaments: departaments
            }

            const departament = new schema.Entity('departament', {}, {idAttribute: 'CODIGO'});

            const schemaStructure = {
                departaments: [departament]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_DEPARTAMENTS,
                payload: {
                    entities,
                    result
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    const actGetCities = async dispatch => {
        try {
            const res = await serGetCities();
            const {data} = res;

            const subCadenaInicio = data.substring(90);
            const newValue = JSON.parse(subCadenaInicio.replace("}</string>", ""));
            const cities = newValue.Datos;

            const schemaData = {
                cities: cities
            }

            const city = new schema.Entity('city', {}, {idAttribute: 'CODIGO'});

            const schemaStructure = {
                cities: [city]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_CITIES,
                payload: {
                    entities,
                    result
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    const actLogout = async dispatch => {
        try {
            dispatch({
                type: LOGIN,
                payload: {}
            })
        } catch (e) {
            console.log(e)
        }
    }

    return {
        actLogin,
        actRegisterUser,
        actGetDepartaments,
        actGetCities,
        actLogout
    }
}

export default useAuthActions;