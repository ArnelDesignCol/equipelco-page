import axios from 'axios';

const useGeneralProvider = () => {
    const proGetSocialLinks = () => {
        return axios.get('/api/general/social_links.json');
    }

    return {
        proGetSocialLinks,
    }
}

export default useGeneralProvider;