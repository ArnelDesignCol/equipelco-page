import useServices from "../../services";
import {normalize, schema} from "normalizr";
import {GET_CONTACT_DETAIL, GET_PAYMENT_METHODS, GET_SERVICES_DETAIL} from "../../types";

const useFooterActions = () => {
    const { useFooterServices } = useServices();
    const { serGetPaymentMethods, serGetContactDetail, serGetServicesDetail } = useFooterServices();

    const actGetPaymentMethods = async dispatch => {
        try {
            const res = await serGetPaymentMethods();
            const { data } = res;

            const schemaData = {
                paymentMethods: data
            }

            const paymentMethod = new schema.Entity('paymentMethod');

            const schemaStructure = {
                paymentMethods: [paymentMethod]
            }

            const { entities, result } = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_PAYMENT_METHODS,
                payload: {
                    entities,
                    result
                }
            })
        } catch (e) {
            console.log(e);
        }
    }

    const actGetContactDetail = async dispatch => {
        try {
            const res = await serGetContactDetail();
            const { data } = res;

            const schemaData = {
                contactDetails: data
            }

            const contactDetail = new schema.Entity('contactDetail');

            const schemaStructure = {
                contactDetails: [contactDetail]
            }

            const { entities, result } = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_CONTACT_DETAIL,
                payload: {
                    entities,
                    result
                }
            })
        } catch (e) {
            console.log(e);
        }
    }

    const actGetServicesDetail = async dispatch => {
        try {
            const res = await serGetServicesDetail();
            const { data } = res;

            const schemaData = {
                servicesDetails: data
            }

            const servicesDetail = new schema.Entity('servicesDetail');

            const schemaStructure = {
                servicesDetails: [servicesDetail]
            }

            const { entities, result } = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_SERVICES_DETAIL,
                payload: {
                    entities,
                    result
                }
            })
        } catch (e) {
            console.log(e);
        }
    }

    return {
        actGetPaymentMethods,
        actGetContactDetail,
        actGetServicesDetail,
    }
}

export default useFooterActions;