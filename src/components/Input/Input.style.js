import styled from "styled-components";
import tw from "twin.macro";

const useInputStyles = () => {
    const StyledInput = styled.input.attrs({
        className: 'Input'
    })`
      &:checked {
        background-color: black;
      }
      
      ${(props) => {
          return props.dark
                  ? "color: white; background-color: black; border: 1px solid white; &::placeholder {color:white;}"
                  : "";
      }}
      
      &:focus {
        outline: 0;
        border: 1px solid #b3b3b3;
      }
      
      ${tw` text-white text-xs rounded-lg bg-transparent border border-white py-4 px-3 font-customNormal`}
    `;

    return {
        StyledInput,
    };
}

export default useInputStyles;