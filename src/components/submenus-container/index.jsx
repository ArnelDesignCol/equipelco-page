import React from 'react';
import {Grid} from "@material-ui/core";
import useStyleSubmenu from "./submenu.style";
import PropTypes from 'prop-types';



const SubmenuLogin = props => {
    const classes = useStyleSubmenu();

    const { open, logout } = props;

    return (
        <Grid item md={3} xs={3} className={classes.container} style={{ display: open ? 'block' : 'none' }}>
            <Grid item xs={12} className="flex justify-center" onClick={logout}>
                <span className="text-white">Cerrar Sesion</span>
            </Grid>
        </Grid>
    );
}

const Submenu = props => {
    const {
        open,
        type,
        logout
    } = props;

    if(type === 'fixedMenu'){
        return (
            <SubmenuLogin open={open} logout={logout}/>
        )
    }
}

export default Submenu;