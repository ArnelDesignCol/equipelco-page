import styled from "styled-components";
import tw from "twin.macro";

const useTabsStyle = () => {
    const StyledTabs = styled.div.attrs({
        className: 'tabs'
    })`
      &.MuiTabs-root{
        min-height: 60px;
        .MuiTabs-indicator {
          ${tw` bg-primary`}
          height: 12px;
          border-radius: 27px;
          @media (max-width: 1024px){
            height: 6px;
          }
        }
      }
    `;

    return {
        StyledTabs,
    };
}

export default useTabsStyle;