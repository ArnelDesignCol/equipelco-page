import useServices from "../../services";
import {normalize, schema} from "normalizr";
import {GET_SOCIAL_LINKS} from "../../types";

const useGeneralActions = () => {
    const {useGeneralServices} = useServices();
    const {serGetSocialLinks} = useGeneralServices();
    const actGetSocialLinks = async dispatch => {
        try {
            const res = await serGetSocialLinks();
            const {data} = res;

            const schemaData = {
                socialLinks: data
            }

            const socialLink = new schema.Entity('socialLink');

            const schemaStructure = {
                socialLinks: [socialLink]
            }

            const {entities, result} = normalize(schemaData, schemaStructure);

            dispatch({
                type: GET_SOCIAL_LINKS,
                payload: {
                    entities,
                    result
                }
            })
        }catch (error){
            console.log(error);
        }
    }

    return {
        actGetSocialLinks,
    }
}

export default useGeneralActions;