import TopBar from "./TopBar/index";
import Header from "./Header/index";
import NavigationTabs from './Tabs/index';
import NavigationTabPanel from './TabPanel/index';
import NavigationTab from "./Tab";
import Footer from "./Footer";
import Carousel from "./Carousel";
import FormRegister from "./FormRegister";
import FixedMenu from "./FixedMenu";

const useComponents = () => {
    return {
        TopBar,
        Header,
        NavigationTabs,
        NavigationTab,
        NavigationTabPanel,
        Footer,
        Carousel,
        FormRegister,
        FixedMenu
    };
}

export default useComponents;